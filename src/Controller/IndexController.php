<?php

namespace App\Controller;

use App\Entity\Favorites;
use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\FavoritesRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/", name="main_page")
     * @throws ApiException
     */
    public function indexAction(ApiContext $apiContext, Request $request)
    {
        $arrayData = null;
        $mainQuery = $apiContext->makePing();
        $arrayData = $mainQuery['data']['children'];

        $currentUser = $this->getUser();
        $form = $this->createForm("App\Form\SearchFilterType");
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $results = $apiContext->takeAll($data);
            $arrayData = null;
            $arrayData = $results['data']['children'];
        }
        return $this->render('main_page.html.twig', [
                'isUser' => $currentUser,
                'form' => $form->createView(),
                'array' => $arrayData
            ]
        );
    }

    /**
     * @Route("/details/{id}", name="details")
     * @Method("GET")
     * @throws ApiException
     */
    public function postDetailsAction($id, ApiContext $apiContext){
        $results = $apiContext->takeOnePost(['id' => $id]);
        $arrayData = $results['data']['children'];
        dump($arrayData);
        return $this->render("details.html.twig", [
            'array' => $arrayData
        ]);
    }

    /**
     * @Route("/add_favorites/{post_name}", name="favorites")
     * @Method("GET")
     */
    public function addFavoritesPostAction($post_name, ObjectManager $objectManager){
        $favoritePost = new Favorites();
        $favoritePost->setPostName($post_name);
        $objectManager->persist($favoritePost);
        $currentUser = $this->getUser();
        $currentUser->addFavorite($favoritePost);
        $favoritePost->addUser($currentUser);
        $objectManager->flush();

        return $this->redirectToRoute('main_page');
    }

    /**
     * @Route("/my_favorites", name="my_favorites")
     * @Method("GET")
     * @throws ApiException
     */
    public function myFavorites(FavoritesRepository $favoritesRepository, ApiContext $apiContext){
        $currentUser = $this->getUser();
        $myFavoritesPosts = $favoritesRepository->getMyFavorites($currentUser);
        $myFavorites = [];
        foreach ($myFavoritesPosts as $value){
            $myFavorites [] = $value->getPostName();
        }
        $queryString = implode(',', $myFavorites);
        $allMyPosts = $apiContext->takeAllMyPosts($queryString);
        $arrayData = $allMyPosts['data']['children'];

        return $this->render('my_favorites.html.twig', [
            'results' => $arrayData
        ]);
    }

    /**
     * @Route("/group_favorites", name="group_favorites")
     * @Method("GET")
     * @throws ApiException
     */
    public function groupFavorites(FavoritesRepository $favoritesRepository,ApiContext $apiContext){
        $myFavoritesPosts = $favoritesRepository->getGroupFavorites();
        $myFavorites = [];
        foreach ($myFavoritesPosts as $value){
            $myFavorites [] = $value->getPostName();
        }
        $queryString = implode(',', $myFavorites);
        $allMyPosts = $apiContext->takeAllMyPosts($queryString);
        $arrayData = $allMyPosts['data']['children'];
        return $this->render("group_favorites.html.twig", [
                'results' => $arrayData
        ]);
    }

    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = [
                'name' => $user->getName(),
                'email' => $user->getEmail(),
                'password' => $user->getPassword()
            ];

            $user = $userHandler->createNewUser($data);
            $manager->persist($user);
            $manager->flush();

            $userHandler->makeUserSession($user);

            return $this->redirectToRoute('app_profile_page');
        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/profile_page", name="app_profile_page")
     * @Method({"POST", "GET"})
     */
    public function profilePageAction(Request $request)
    {
        $currentUser = $this->getUser();
        return $this->render('profile_page.html.twig', ['user' => $currentUser]);
    }


    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);

                return $this->redirectToRoute('main_page');
            }

            try {
                $error = 'Ты не тот, за кого себя выдаешь';
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }
}