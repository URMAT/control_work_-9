<?php

namespace App\Repository;

use App\Entity\Favorites;
use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favorites|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorites|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorites[]    findAll()
 * @method Favorites[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoritesRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry,
        UserHandler $userHandler)
    {
        parent::__construct($registry, Favorites::class);
        $this->userHandler = $userHandler;
    }

    public function getCountFavorites($note_id) {
        return $this->createQueryBuilder('n')
            ->select('COUNT(n) amount')
            ->join('n.users', 'u')
            ->where('n.id = :id')
            ->setParameter('id', $note_id)
            ->getQuery()->getResult();
    }

    public function getMyFavorites(User $user) {
        return $this->createQueryBuilder('p')
            ->select('COUNT(u) AS HIDDEN posts', 'p')
            ->leftJoin('p.users', 'u')
            ->where('u = :user')
            ->setParameter('user', $user)
            ->orderBy('posts', 'DESC')
            ->groupBy('p')
            ->getQuery()
            ->getResult();
    }

    public function getGroupFavorites(User $user = null) {
        $qb = $this->createQueryBuilder('p')
            ->select('COUNT(u) AS HIDDEN posts', 'p')
            ->leftJoin('p.users', 'u');
        if(!empty($user)){
            $qb->where('u = :user')
            ->setParameter('user', $user);
        }
        return $qb->orderBy('posts', 'DESC')
            ->groupBy('p')
            ->getQuery()
            ->getResult();
    }
}
