<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'q',
                TextType::class, [
                    'label' => 'Что ищем?',
                ]
            )
            ->add('sort', ChoiceType::class, [
                'label' => 'Сортировать по',
                'choices' => [
                    'Горячие' => 'hot',
                    'В топе' => 'top',
                    'Новые' => 'new',
                    'Комментируемые' => 'comments',
                    'По релевантности' => 'relevance'
                ],
            ])
            ->add('limit', ChoiceType::class, [
                'label' => 'Количество',
                'placeholder' => 'Сколько?',
                'choices' => [
                    '8шт' => 8,
                    '12шт' => 12,
                    '16шт' => 16,
                    '50шт' => 50,
                    '100шт' => 100
                ],
            ])
            ->add('Search', SubmitType::class, [
                'label' => 'Искать'
            ])
        ;
    }
}
