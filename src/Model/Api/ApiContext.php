<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/r/picture/search.json?q=cat&sort=new&limit=10&type=link';
    const ENDPOINT_R_PICTURE_SEARCH = '/r/picture/search.json?';
    const ENDPOINT_API_INFO = '/api/info.json?';
    const ENDPOINT_TAKE_MY_POSTS = '/api/info.json?';


    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function takeAll($data)
    {
        return $this->makeQuery(self::ENDPOINT_R_PICTURE_SEARCH, self::METHOD_GET, $data);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function takeOnePost($data)
    {
        return $this->makeQuery(self::ENDPOINT_API_INFO, self::METHOD_GET, $data);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function takeAllMyPosts($data)
    {

        return $this->makeQuery(self::ENDPOINT_TAKE_MY_POSTS, self::METHOD_GET, ['id' => $data]);
    }
}
