<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Entity\Favorites;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavoritesFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        $post = new Favorites();
        $post->setPostName('t3_8k3o4v');
        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER1);
        $post->addUser($user1);
        $manager->persist($post);

        $post1 = new Favorites();
        $post1->setPostName('t3_8vtwwx');
        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER1);
        $post1->addUser($user1);
        $manager->persist($post1);

        $post6 = new Favorites();
        $post6->setPostName('t3_8unv2t');
        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER1);
        $post6->addUser($user1);
        $manager->persist($post6);

        $post7 = new Favorites();
        $post7->setPostName('t3_8l1w7k');
        /** @var User $user1 */
        $user1 = $this->getReference(UserFixtures::USER1);
        $post7->addUser($user1);
        $manager->persist($post7);


        $post3 = new Favorites();
        $post3->setPostName('t3_8vtwwx');
        /** @var User $user1 */
        $user2 = $this->getReference(UserFixtures::USER2);
        $post3->addUser($user2);
        $manager->persist($post3);

        $post4 = new Favorites();
        $post4->setPostName('t3_8unv2t');
        /** @var User $user1 */
        $user2 = $this->getReference(UserFixtures::USER2);
        $post4->addUser($user2);
        $manager->persist($post4);

        $post5 = new Favorites();
        $post5->setPostName('t3_8l1w7k');
        /** @var User $user1 */
        $user2 = $this->getReference(UserFixtures::USER2);
        $post5->addUser($user2);
        $manager->persist($post5);

        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }

}
