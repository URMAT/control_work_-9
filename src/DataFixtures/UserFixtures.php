<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public const USER1 = 'user1';
    public const USER2 = 'user2';
    public const USER3 = 'user3';


    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {

        $user1 = $this->userHandler->createNewUser([
            'name' => 'User',
            'email' => 'user1@gmail.com',
            'password' => '123',
            'roles' => ["ROLE_USER"],
        ]);

        $this->addReference(self::USER1, $user1);
        $manager->persist($user1);

        $user2 = $this->userHandler->createNewUser([
            'name' => 'User 2',
            'email' => 'user2@gmail.com',
            'password' => '123',
            'roles' => ["ROLE_USER"],
        ]);
        $this->addReference(self::USER2, $user2);

        $manager->persist($user2);

        $user3 = $this->userHandler->createNewUser([
            'name' => 'User 3',
            'email' => 'user3@gmail.com',
            'password' => '123',
            'roles' => ["ROLE_USER"],
        ]);

        $this->addReference(self::USER3, $user3);
        $manager->persist($user3);
        $manager->flush();
    }
}
