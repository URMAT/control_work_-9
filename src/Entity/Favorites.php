<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FavoritesRepository")
 */
class Favorites
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=1024)
     */
    private $post_name;

    /**
     * @var ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\User", inversedBy="favorite")
     */
    private $users;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $data;

    public function __construct()
    {
        $this->data = new \DateTime("now");
        $this->users = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $post_name
     * @return Favorites
     */
    public function setPostName(string $post_name): Favorites
    {
        $this->post_name = $post_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getPostName(): string
    {
        return $this->post_name;
    }

    /**
     * @param mixed $user
     * @return Favorites
     */
    public function setUsers($user)
    {
        $this->users = $user;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @return mixed
     */
    public function addUser(User $user)
    {
        return $this->users->add($user);
    }


}
